package model.data_structures;

import java.util.Iterator;

public interface IHash<K, V> extends Iterable<K>{

	/*
	 * Agregar	 una	 dupla	 (K,	 V)	 a	 la	 tabla.	 Si	 la	 llave	 K	
	 * existe,	 se	 reemplaza	 su	 valor	 V	 asociado.	 V	 no	
	 * puede ser null.
	 */
	public void put(V v ,K k);
	
	/*
	 * Obtiene el valor V asociado a la llave K. V no puede ser null
	 */
	public V get(K k);
	
	/*
	 * Borra la dupla asociada a la llave K. Se obtiene el valor V asociado a la
	 * llave K. Se obtiene null si la llave K no existe.
	 */
	public V delete(K k);
	
	/*
	 * Conjunto de llaves K presentes en la tabla
	 */
	public Iterator<K> keys();
}
